// ==UserScript==
// @version      1.1.0
// @name         Patches for Mazda service manuals
// @namespace    https://www.azabani.com/
// @author       Delan Azabani <delan@azabani.com>
// @match        https://mazdamanuals.com.au/wsm-secure/WSM/*
// @grant        none
// ==/UserScript==

(function() {
	"use strict";

	function each(action) {
		var rest = Array.from(arguments).slice(1);
		this.forEach(function(x) {
			action.apply(x, rest);
		});
	}

	function patch(subject, property) {
		var old = subject[property].toString();
		var rest = Array.from(arguments).slice(2);
		var code = old.replace.apply(old, rest);
		// works with function declarations and expressions
		subject[property] = subject.eval("(" + code + ")");
	}

	function $(subject) {
		"use strict";
		if (this != undefined) {
			Object.keys($).forEach(function(x) {
				this[x] = each.bind(subject, $[x]);
			}, this);
		} else if (Object(subject) instanceof $) {
			return $;
		} else if (Object(subject) instanceof Array) {
			return new $(subject);
		} else if (Object(subject) instanceof String) {
			var that = this;
			if (arguments.length > 2)
				that = arguments[2];
			subject = document.querySelectorAll(subject);
			if (arguments.length > 1)
				Array.from(subject, arguments[1], that);
			return $(Array.from(subject));
		} else {
			return $([subject]);
		}
	}

	$.on = function(event, action) {
		var capturing = false;
		if (arguments.length > 2)
			capturing = arguments[2];
		this.addEventListener(event, action, capturing);
	};

	$.frame = function(action) {
		var rest = Array.from(arguments).slice(1);
		$(this).on("load", function(x) {
			action.apply(x.target.contentWindow, rest);
		});
	};

	$("[name=srvc_menu]").frame(function() {
		// frames() no longer works
		patch(this, "ChangeSrvc", /[.]frames[(]([^)]+)[)]/g, '.frames[$1]');
	});

	$("[name=left_menu]").frame(function() {
		// Firefox needs these hacks
		patch(this, "Toc_click", /^(\s*function\s*Toc_click\s*)\(\s*\)/g, '$1(event)');
		patch(this, "Toc_click", /window\.event\.srcElement/g, 'event.target');
		patch(this, "mouse_over", /^(\s*function\s*mouse_over\s*)\(\s*\)/g, '$1(event)');
		patch(this, "mouse_over", /window\.event\.srcElement/g, 'event.target');

		// Firefox needs this too
		this.CSSStyleSheet.prototype.addRule = function(selector, body) {
			this.insertRule(selector + "{" + body + "}", this.cssRules.length);
		};

		// document.all.tags() no longer works
		this.document.all.tags = document.querySelectorAll.bind(document);

		// frames() no longer works
		patch(this, "Navigate_URL", /[.]frames[(]([^)]+)[)]/g, '.frames[$1]');
		patch(this, "Image_Click", /[.]frames[(]([^)]+)[)]/g, '.frames[$1]');

		// frames[] seems to miss <iframe> elements
		patch(this, "Image_Click", /document[.]frames\["hiddenframe"\]/g, 'document.all["hiddenframe"].contentWindow');

		// all[] no longer works on elements
		patch(this, "Image_Click", /eUL\.all\.length/g, 'eUL.querySelectorAll("*").length');

		// provide missing <ul> elements
		this.GetNextUL = (function(x) {
			var result = x.querySelector('ul');
			if (result == null) {
				result = this.document.createElement('ul');
				x.appendChild(result);
			}
			return result;
		}).bind(this);

		// addRule was of object type on IE at the time
		patch(this, "window_load", /object" == typeof\( objStyle\.addRule/, 'function" == typeof( objStyle.addRule');

		// improve some styles
		this.document.styleSheets[0].addRule("iframe[name=hiddenframe]", "display: none;");
		this.document.styleSheets[0].addRule("ul#ulRoot, ul.clsShown", "display: block; padding: 0;");

		// fire the load event again with our patched listener
		this.window_load();
	});
})();
