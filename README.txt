This user script makes Mazda’s service manuals work in browsers like
Chrome and Firefox. Get around it:

1. install Greasemonkey (Firefox) [0] or Tampermonkey (Chrome) [1]
2. go to https://www.azabani.com/labs/mazuals/script.user.js

Mazda’s service manuals are pretty broken on any browser other than
Internet Explorer, because it was written for IE 4, in the dark days
of the two kinds of “Intermediate DOM” [2]. Do “document.layers” and
“document.all” ring a bell? The browser war that culminated in IE 4
and Netscape 4 [3] was the genesis of the Web Standards Project [4].

When they work, the menus are pretty fancy. They fetch their items
as you expand their parents, and they only fetch each item once.

IE 5 and Netscape 5 started implementing the new “W3C DOM”, but the
latter was cancelled, so Netscape begat Mozilla, and to simplify the
history a bit, Mozilla begat Firefox. IE 5 was also the version that
introduced a little feature you might have heard of: XMLHttpRequest.

The point I was originally trying to make is that Mazda’s manuals
have these “DHTML” menus that use lazy loading, for a browser that
didn’t even have creature comforts like XMLHttpRequest, and I feel
like that’s pretty cool. Enough rose-tinted glasses for one day.

[0] https://addons.mozilla.org/firefox/addon/greasemonkey/
[1] https://chrome.google.com/webstore/detail/dhdgffkkebhmkfjojejmpbldmpobfkfo
[2] http://www.quirksmode.org/js/dom0.html
[3] https://en.wikipedia.org/wiki/Browser_wars#First_browser_war
[4] http://www.webstandards.org/about/history/
